/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  Implementation of most encoding/decoding methods
 */


#include "bwted.h"
#include "Huffman.h"
#include "BitWritter.h"

#include <cstdio>
#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include <climits>
#include <sstream>
#include <bitset>

#include <iostream>

#define LINE_FEED 10
#define ALG_ERROR 1
#define RLE_DELIM 4 // End of transmission
#define BLOCK_SIZE 8192
#define KEY_IDENTIFY 5

using namespace std;

struct tRBWT {
    uint8_t symbol;
    uint32_t match;
};

string releehWsworrub(string input);
string burrowsWheeler(string input);
string tnorFoTevom(string input);
string moveToFront(string input);
string elr(string input);
string rle(string input);
string namffuh(string input);
vector<uint8_t> huffman(string input);

/**
 *  ____                                   
 * | __ ) _   _ _ __ _ __ _____      _____ 
 * |  _ \| | | | '__| '__/ _ \ \ /\ / / __|
 * | |_) | |_| | |  | | | (_) \ V  V /\__ \
 * |____/ \__,_|_|  |_|  \___/ \_/\_/ |___/
 *                                        
 *  __        ___               _           
 *  \ \      / / |__   ___  ___| | ___ _ __ 
 *   \ \ /\ / /| '_ \ / _ \/ _ \ |/ _ \ '__|
 *    \ V  V / | | | |  __/  __/ |  __/ |   
 *     \_/\_/  |_| |_|\___|\___|_|\___|_|   
 *                                       
 *
 * @param bwted
 * @param inputFile
 * @param outputFile
 * @return 0 if ok otherwise 1
 */

int BWTEncoding(tBWTED* bwted, FILE* inputFile, FILE* outputFile) {
    string data;
    string buffer = "";
    int c;
    size_t currentSize = 0;
    while (true) {
        c = fgetc(inputFile);
        if (c != EOF) {
            bwted->uncodedSize++;
            buffer += static_cast<uint8_t> (c);
            currentSize++;
        }
        if ((currentSize == BLOCK_SIZE - 1) || c == EOF) {
            currentSize = 0;
            data += burrowsWheeler(buffer);
            buffer = "";
            if (c == EOF) {
                break;
            }
        }
    }
    string afterMtF = moveToFront(data);
    string afterRLE = rle(afterMtF);
    vector<uint8_t> afterHuffman = huffman(afterRLE);
    bwted->codedSize = afterHuffman.size();
    fwrite(&afterHuffman[0], sizeof (uint8_t), afterHuffman.size(), outputFile);

    //ready to write
    return 0;
}

/**
 *                                    ____  
 *  _____      _____  _ __ _ __ _   _| __ ) 
 * / __\ \ /\ / / _ \| '__| '__| | | |  _ \ 
 * \__ \\ V  V / (_) | |  | |  | |_| | |_) |
 * |___/ \_/\_/ \___/|_|  |_|   \__,_|____/ 
 *                                         
 *           _           _  __        __
 *  _ __ ___| | ___  ___| |_\ \      / /
 * | '__/ _ \ |/ _ \/ _ \ '_ \ \ /\ / / 
 * | | |  __/ |  __/  __/ | | \ V  V /  
 * |_|  \___|_|\___|\___|_| |_|\_/\_/   
 *                                      
 * 
 * @param bwted
 * @param inputFile
 * @param outputFile
 * @return if ok, otherwise 1
 */
int BWTDecoding(tBWTED* bwted, FILE* inputFile, FILE* outputFile) {
    string data;
    int c;
    while ((c = fgetc(inputFile)) != EOF) {
        bwted->codedSize++;
        data += static_cast<uint8_t> (c);
    }

    string beforeHuffman = namffuh(data);
    string beforeRLE = elr(beforeHuffman);
    string beforeMTF = tnorFoTevom(beforeRLE);
    size_t size;
    string result = "";
    string buffer;
    while (!beforeMTF.empty()) {
        size = (beforeMTF.size() >= BLOCK_SIZE) ? BLOCK_SIZE : beforeMTF.size();
        buffer = beforeMTF.substr(0, size);
        buffer = releehWsworrub(buffer);
        result += buffer;
        beforeMTF.erase(0, size);
    }
    fwrite(result.c_str(), sizeof (uint8_t), result.size(), outputFile);
    bwted->uncodedSize = result.size();
    return 0;
}

/**
 * Static huffman binary encoding of simple string.
 * @param input string of data to encode
 * @return vector of bytes ready to be written into file
 */
vector<uint8_t> huffman(string input) {
    Huffman huffman;
    BitWritter bitWritter;

    for (char & c : input) {
        string number = huffman.encode(static_cast<uint8_t> (c));
        bitWritter.write(number);
    }

    return bitWritter.getData();
}

/**
 * Reverse static huffman
 * decode simple string of binary data into simple string of decoded huffman
 * data
 * @param input string of data to decode
 * @return string of decoded data
 */
string namffuh(string input) {
    Huffman huffman;
    string output = "";
    string number = "";
    uint8_t decode;
    string currentByte;
    for (char & c : input) {
        bitset<8> byte(static_cast<uint8_t> (c));
        currentByte = byte.to_string();
        for (size_t i = 0; i < 8; i++) {
            number += currentByte[i];
            if (huffman.includeSize(number.size())) {
                try {
                    decode = huffman.decode(number);
                    output += decode;
                    number = "";
                } catch (const out_of_range& ex) {
                    //nothing to do
                }
            }
        }
    }
    return output;
}

/**
 * Run Length Encoding
 * Converts series of characters into 2 byte pairs <character><num of occurences>
 * ALL of the characters is coded. In worst case scenario result is 2-times
 * bigger!
 * @param input string of data to be encoded
 * @return RLE encoded string of pairs
 */
string rle(string input) {
    string result = "";
    uint8_t lastChar = input[0];
    int num = -1;
    uint8_t uc;
    for (char & c : input) {
        uc = static_cast<uint8_t> (c);
        if (uc == lastChar && num < UCHAR_MAX) {
            num++;
        } else {
            result += lastChar;
            result += static_cast<uint8_t> (num);
            lastChar = uc;
            num = 0;
        }
    }

    //fix for last character
    result += lastChar;
    result += static_cast<uint8_t> (num);

    return result;
}

/**
 * Run Length Decoding
 * Converts serios of 2 byte pairs into decoded data.
 * Will fail in case if string isn't even sized 
 * @param input series of 2 byte pairs
 * @return decoded string
 */
string elr(string input) {
    string result = "";

    int state = 0;
    uint8_t character;
    uint8_t num;
    for (char & c : input) {
        if (state == 0) {
            character = c;
            state++;
        } else if (state == 1) {
            num = (static_cast<uint8_t> (c) + 1);
            result.insert(result.end(), num, character);
            state = 0;
        }
    }

    return result;
}

/**
 * Move to Front transformation
 * @param input data to be encoded by MtF transformation
 * @return transformed data
 */
string moveToFront(string input) {
    vector<uint8_t> dictionary;
    vector<uint8_t>::iterator it;
    string result = "";

    for (int c = 0; c < UCHAR_MAX; c++) {
        dictionary.push_back(static_cast<uint8_t> (c));
    }

    int8_t symbol;
    for (char & c : input) {
        it = find(dictionary.begin(), dictionary.end(), static_cast<uint8_t> (c));
        symbol = *it;
        result += static_cast<uint8_t> (it - dictionary.begin());
        dictionary.erase(it);
        dictionary.insert(dictionary.begin(), symbol);
    }

    return result;
}

/**
 * Reverse Move to Front transformation
 * @param input data to be decoded by MtF transformation
 * @return transformed data
 */
string tnorFoTevom(string input) {
    vector<uint8_t> dictionary;
    string result = "";

    for (uint32_t c = 0; c < UCHAR_MAX; c++) {
        dictionary.push_back(static_cast<uint8_t> (c));
    }

    uint8_t symbol;
    for (char & c : input) {
        symbol = dictionary[static_cast<uint8_t> (c)];
        result.push_back(symbol);
        dictionary.erase(dictionary.begin() + static_cast<uint8_t> (c));
        dictionary.insert(dictionary.begin(), symbol);
    }

    return result;
}

/**
 * Burrows Wheeler transformation.
 * Block size is given by string size.
 * Increase size by tail character ASCII character decadic '5'
 * @param input string to be transformed by burrows wheeler
 * @return transformed string with tail character
 */
string burrowsWheeler(string input) {
    vector<string> variations;
    input.push_back(KEY_IDENTIFY);
    variations.push_back(input);
    for (size_t i = 1; i < input.length(); i++) {
        variations.push_back(variations[i - 1]);
        rotate(variations[i].begin(), variations[i].begin() + 1, variations[i].end());
    }
    sort(variations.begin(), variations.end(), less<>());

    string result = "";
    for (size_t i = 0; i < variations.size(); i++) {
        result.push_back(variations[i].back());
    }
    return result;
}

/**
 * Reverse Burrows Wheeler transformation
 * Block size is given by string size.
 * Block needs to include tail character (decadic '5')
 * @param input string to be reverse transformed
 * @return transformed string without tail character
 */
string releehWsworrub(string input) {
    vector<tRBWT> precedingSymbols; //table1
    map<uint8_t, uint32_t> symbolsCount;
    map<uint8_t, uint32_t> symbolsLessThan; //table2
    map<uint8_t, uint32_t>::iterator it;
    //table1
    for (char & c : input) {
        tRBWT precTable;
        precTable.match = 0;
        precTable.symbol = static_cast<uint8_t>(c);
        it = symbolsCount.find(precTable.symbol);
        if (it == symbolsCount.end()) {
            precTable.match = 0;
            symbolsCount.insert(pair<uint8_t, uint32_t>(precTable.symbol, 0));
        } else {
            it->second++;
            precTable.match = it->second;
        }
        precedingSymbols.push_back(precTable);
    }
    

    //table2
    uint32_t sum = 0;
    for (it = symbolsCount.begin(); it != symbolsCount.end(); it++) {
        if (it != symbolsCount.begin()) {
            sum += prev(it)->second + 1;
        }
        symbolsLessThan.insert(pair<uint8_t, uint32_t>(it->first, sum));
    }
    

    string result = "";
    tRBWT lastSymbol;
    for(tRBWT sym : precedingSymbols) {
        if(sym.symbol == KEY_IDENTIFY) {
            lastSymbol = sym;
            break;
        }
    }
    for (size_t i = input.length() - 1; i > 0; i--) {
        lastSymbol = precedingSymbols[lastSymbol.match + symbolsLessThan[lastSymbol.symbol]];
        result.insert(0, 1, lastSymbol.symbol);
    }

    return result;
}
