/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  Implementation of most encoding/decoding methods
 */


#ifndef BWTED_H
#define BWTED_H

#include <cstdlib>
#include <cstdio>


struct tBWTED {
    int64_t uncodedSize;
    int64_t codedSize;
};

/**
 * Encode file
 * @param bwted -
 * @param inputFile
 * @param outputFile
 * @return 0 if success otherwise -1
 */
int BWTEncoding(tBWTED *bwted, FILE *inputFile, FILE *outputFile);

/**
 * Decode file
 * @param ahed
 * @param inputFile
 * @param outputFile
 * @return 0 if sucess otherwise -1
 */
int BWTDecoding(tBWTED *ahed, FILE *inputFile, FILE *outputFile);


#endif /* BWTED_H */
