CC=g++
CFLAGS=-Wall -Wextra -pedantic -std=c++14
CLIBS=
BIN=bwted
OBJECTS=bwted.o Huffman.o BitWritter.o

.PHONY: clean run

all: clean main.o $(OBJECTS)
	$(CC) $(CFLAGS) main.o $(OBJECTS) $(CLIBS) -o $(BIN) 

main.o:
	$(CC) $(CFLAGS) -c main.cpp
bwted.o:
	$(CC) $(CFLAGS) -c bwted.cpp
Huffman.o:
	$(CC) $(CFLAGS) -c Huffman.cpp    
BitWritter.o:
	$(CC) $(CFLAGS) -c BitWritter.cpp
clean:
	rm -f *.o $(BIN)
run:
	./$(BIN)
