/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  Implementation of Huffman class
 */

#include "Huffman.h"
#include <fstream>
#include <iostream>

#include <algorithm>

#define HUFFMAN_DATA "huffman_encode.txt"

using namespace std;

Huffman::Huffman() {
    ifstream file(HUFFMAN_DATA);
    
    int pos;
    char delimeter;
    string code;
    while((file >> pos >> delimeter >> code) && delimeter == ',') {
        sizes.insert(code.size());
        decodeMap.insert(pair<string, unsigned char>(code, pos));
        encodeMap.insert(pair<unsigned char, string>(pos, code));
    }
    
    file.close();
}

Huffman::~Huffman() {
    
}

unsigned char Huffman::decode(const string code) const {
    return decodeMap.at(code);
}

string Huffman::encode(const unsigned char character) const {
    return encodeMap.at(character);
}

bool Huffman::includeSize(const size_t size) const {
    return (sizes.find(size) != sizes.end());
}
