/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  main application
 */

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <ctype.h>

#include "bwted.h"

using namespace std;

void printUsage() {
    cerr << "Run as bwted -i <input file> -o <output file> <-c|-x for coding/decoding> [-l <log file>]" << endl;
}

int main(int argc, char** argv) {
    
    char *inputFile = NULL;
    char *outputFile = NULL;
    char *logFile = NULL;
    int operation = 0;

    int c;
    opterr = 0;

    while ((c = getopt(argc, argv, "i:o:l:cxh")) != -1) {
        switch (c) {
            case 'i':
                inputFile = optarg;
                break;
            case 'o':
                outputFile = optarg;
                break;
            case 'l':
                logFile = optarg;
                break;
            case 'c':
                if(operation) {
                    cerr << "You cant define both encode and decode param" << endl;
                    return 2;
                }
                operation = 1;
                break;
            case 'x':
                if(operation) {
                    cerr << "You cant define both encode and decode param" << endl;
                    return 2;
                }
                operation = 2;
                break;
            case 'h':
                printUsage();
                return 0;
                break;
            case '?':
                if (optopt == 'i' || optopt == 'o' || optopt == 'l')
                    cerr << "Option -" << c << " requires an argument" << endl;
                else if (isprint(optopt))
                    cerr << "Unknown option -" << static_cast<char>(c) << endl;
                else
                    cerr << "Unkown option character '" << optopt << "'" << endl;
                return 2;
            default:
                return 2;
                break;
        }
    }
    
    if(!operation) {
        cerr << "Operation not specified please include -c or -x argument" << endl;
        return 2;
    }

    FILE* input = (inputFile == NULL) ? stdin : fopen(inputFile, "r");
    FILE* output = (outputFile == NULL) ? stdout : fopen(outputFile, "w");
    tBWTED data;
    int result;
    
    if(operation == 1) {
        result = BWTEncoding(&data, input, output);
    } else {
        result = BWTDecoding(&data, input, output);
    }
    
    fclose(input);
    fclose(output);
    if(logFile != NULL) {
        FILE* log = fopen(logFile, "w");
        fprintf(log, "login = xvanku00\nuncodedSize = %ld\ncodedSize = %ld\n", data.uncodedSize, data.codedSize);
        fclose(log);
    }
    return result;
}

