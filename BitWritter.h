/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  BitWritter class is used for writing binary data to byte data
 */


#ifndef BITWRITTER_H
#define BITWRITTER_H

#include <vector>
#include <string>

class BitWritter {
private:
    std::vector<uint8_t> data;
    size_t pos = 0;
    uint8_t temp;
public:
    BitWritter();
    virtual ~BitWritter();

    /**
     * Write string of bits ASCII characters 1 and 0
     * @param bits to write into binary
     */
    void write(std::string bits);
    
    /**
     * @return vector of bytes ready to be written into file
     */
    std::vector<uint8_t> getData();

};

#endif /* BITWRITTER_H */

