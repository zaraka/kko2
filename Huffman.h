/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  Huffman class is used for encoding/decoding byte to static Huffman
 *  code (code is specified by external file).
 */


#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <string>
#include <map>
#include <climits>
#include <vector>
#include <set>

class Huffman {
private:
    std::map<unsigned char, std::string> encodeMap;
    std::map<std::string, unsigned char> decodeMap;
    std::set<size_t> sizes;
public:
    Huffman();
    virtual ~Huffman();
    
    /**
     * Encode character to string of '1' and '0'
     * @param character to be encooded
     * @return string of '1' and '0'
     */
    std::string encode(const unsigned char character) const;
    
    /**
     * Decode string of '1' and '0' to character
     * @param code string of '1' and '0'
     * @return decoded character
     */
    unsigned char decode(const std::string code) const;
    
    /**
     * Is this size in huffman tree?
     * @param size to be checked
     * @return true if tree includes at least 1 huffman code with same size
     * 0 otherwise
     */
    bool includeSize(const size_t) const;

};

#endif /* HUFFMAN_H */

