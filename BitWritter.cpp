/*  author: Nikita Vanku xvanku00
 *  created: 9.3.2017
 *  Implementation of BitWritter class
 */


#include "BitWritter.h"

using namespace std;

BitWritter::BitWritter() {
}

BitWritter::~BitWritter() {
}

void BitWritter::write(std::string bits) {
    for (char & bit : bits) {
         temp = temp << 1;
         if(bit != '0') {
             temp++;
         }
         pos++;
         if(pos > 7) {
             pos = 0;
            data.push_back(temp);
            temp = 0;
         }
     }
}

std::vector<uint8_t> BitWritter::getData() {
    if(pos != 0) {
        while(pos < 8) {
            temp = temp << 1;
            pos++;
        }
        data.push_back(temp);
        pos = 0;
        temp = 0;
    }
    
    return data;
}
